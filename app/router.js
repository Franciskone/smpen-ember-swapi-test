import EmberRouter from '@ember/routing/router';
import config from './config/environment';


export var ROUTE_NAME = {
  PLAY: 'play',
};

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('play');
});

export default Router;
