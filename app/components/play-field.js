import { computed } from '@ember/object';
import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  
  peopleService: service('people'),
  starshipsService: service('starships'),
  wincounterService: service('wincounter'),
  
  peopleList: computed('peopleService.peopleList', function() { return this.peopleService.peopleList; }),
  starshipsList: computed('starshipsService.starshipsList', function() { return this.starshipsService.starshipsList; }),
  
  playerOneWinAmount: computed('wincounterService.playerOne', function() { return this.wincounterService.playerOne; }),
  playerTwoWinAmount: computed('wincounterService.playerTwo', function() { return this.wincounterService.playerTwo; }),
  
  isDataReady: computed('peopleList', 'starshipsList', isDataReady),
  currentGame: null,
  
  actions: {
    updateCurrentGameWithPeople() {
      const newGame = getNewPeopleGame(this.peopleList);
      
      if(newGame.playerOne.isWinner && !newGame.playerTwo.isWinner) {
        this.wincounterService.playerOneIncrease()
      }
      else if(!newGame.playerOne.isWinner && newGame.playerTwo.isWinner) {
        this.wincounterService.playerTwoIncrease()
      }
      else if(newGame.playerOne.isWinner && newGame.playerTwo.isWinner) {
        this.wincounterService.playerOneIncrease();
        this.wincounterService.playerTwoIncrease();
      }
      
      // console.log(JSON.stringify(newGame));
      this.set('currentGame', newGame);
    },
    updateCurrentGameWithStarships() {
      const newGame = getNewStarShipsGame(this.starshipsList);
      
      if(newGame.playerOne.isWinner && !newGame.playerTwo.isWinner) {
        this.wincounterService.playerOneIncrease()
      }
      else if(!newGame.playerOne.isWinner && newGame.playerTwo.isWinner) {
        this.wincounterService.playerTwoIncrease()
      }
      else if(newGame.playerOne.isWinner && newGame.playerTwo.isWinner) {
        this.wincounterService.playerOneIncrease();
        this.wincounterService.playerTwoIncrease();
      }
      
      this.set('currentGame', newGame);
    }
  },

});



function isDataReady() {
  return !!this.peopleList.length && !!this.starshipsList.length;
}

function getNewPeopleGame(opponentsList) {
  const isEmptyList = !opponentsList.length;
  
  const output = {
    isPeopleType: true,
    playerOne: null,
    playerTwo: null,
  };
  
  if(!isEmptyList) {
    const playerIndexes = getRandomPlayIndexes(opponentsList.length);
    const playerOneData = opponentsList[playerIndexes.one];
    const playerTwoData = opponentsList[playerIndexes.two];
    
    const playerOneMass = parseInt(playerOneData.mass);
    const playerTwoMass = parseInt(playerTwoData.mass);
    
    const isPlayerOneWinner = playerOneMass > playerTwoMass;
    const isDraw = playerOneMass === playerTwoMass;
    
    output.playerOne = {
      imageUrl: "https://cdn-images-1.medium.com/max/1200/0*VtQec_vkEa0kpkl7.png",
      // imageUrl: "http:/www.francescocappelli.it/test/smpen-swapi-test-img/player-one.jpg",
      data: playerOneData,
      isWinner: isPlayerOneWinner || isDraw,
    };
    output.playerTwo = {
      imageUrl: "http://3.bp.blogspot.com/-XO8s8Ax6M7w/UAZq3kTGK5I/AAAAAAAAA4I/hM77zr0rE40/s400/darth-vader-wallpaper-400x300.jpg",
      // imageUrl: "assets/images/player-one.jpg",
      data: playerTwoData,
      isWinner: !isPlayerOneWinner || isDraw,
    };
  }
  
  // console.log(output);
  return output;
}

function getNewStarShipsGame(opponentsList) {
  const isEmptyList = !opponentsList.length;
  
  const output = {
    isPeopleType: false,
    playerOne: null,
    playerTwo: null,
  };
  
  if(!isEmptyList) {
    const playerIndexes = getRandomPlayIndexes(opponentsList.length);
    const playerOneData = opponentsList[playerIndexes.one];
    const playerTwoData = opponentsList[playerIndexes.two];
  
    const playerOneCrew = parseInt(playerOneData.crew);
    const playerTwoCrew = parseInt(playerTwoData.crew);
  
    const isPlayerOneWinner = playerOneCrew > playerTwoCrew;
    const isDraw = playerOneCrew === playerTwoCrew;
    
    
    output.playerOne = {
      imageUrl: "https://media.golfdigest.com/photos/5ad4e8c370e68e282f66ed88/master/w_768,c_limit/falcon.png",
      data: playerOneData,
      isWinner: isPlayerOneWinner || isDraw,
    };
    output.playerTwo = {
      imageUrl: "https://lumiere-a.akamaihd.net/v1/images/vaders-tie-fighter_8bcb92e1.jpeg?region=0%2C147%2C1560%2C878&width=1536",
      data: playerTwoData,
      isWinner: !isPlayerOneWinner || isDraw,
    };
  }
  
  // console.log(output);
  return output;
}

function getRandomPlayIndexes(listLength) {
  const indexOne = Math.floor(Math.random()*listLength);
  let indexTwo = Math.floor(Math.random()*listLength);
  
  return {
    one: indexOne,
    two: indexTwo,
  }
}
