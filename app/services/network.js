import Service from '@ember/service';
import config from '../config/environment';

export default Service.extend({
  swapiRoot: null,
  init() {
    this._super(...arguments);
    this.set('swapiRoot', config.swapiRoot);
  },
  getTotalResults(nextUrl, prevResults = []) {
    const currentResults = [...prevResults];
    
    return fetch(nextUrl)
    .then(res => res.json())
    .then(data => {
      currentResults.push(...data.results);
      
      const onlyFirstPage = true;
      
      return !!data.next && !onlyFirstPage
        ? this.getTotalResults(data.next, currentResults)
        : currentResults;
    })
    .catch(err => {
      // console.err(err);
      // return the results collected until the error
      return err;
    });
  },
  getSwapiPeople() {
    const baseUrl = this.get('swapiRoot');
    return this.getTotalResults(`${baseUrl}people`);
  },
  getSwapiStarships() {
    const baseUrl = this.get('swapiRoot');
    return this.getTotalResults(`${baseUrl}starships`);
  },
});
