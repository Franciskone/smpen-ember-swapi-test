import Service, { inject } from '@ember/service';

export default Service.extend({
  networkService: inject('network'),
  
  starshipsList: null,
  addStarships,
  init() {
    this._super(...arguments);
    this.set('starshipsList', []);
    
    this.networkService.getSwapiStarships()
      .then(starships => {
        !this.isDestroyed
          ? this.addStarships(starships)
          : null;
      });
  },
});

function addStarships(newStarshipsList = []) {
  const currentList = this.get('starshipsList');
  this.set('starshipsList', [...currentList, ...newStarshipsList]);
}
