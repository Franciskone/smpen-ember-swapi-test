import Service, { inject } from '@ember/service';

export default Service.extend({
  networkService: inject('network'),
  peopleList: null,
  
  init() {
    this._super(...arguments);
    const onlyFirstPageResults = true;
    this.set('peopleList', []);
  
    this.networkService.getSwapiPeople(onlyFirstPageResults)
    .then(people => {
      !this.isDestroyed
      ? this.addPeople(people)
      : null;
    });
    
  },
  addPeople,
});

function addPeople(newPeopleList = []) {
  const currentList = this.get('peopleList');
  this.set('peopleList', [...currentList, ...newPeopleList]);
}
