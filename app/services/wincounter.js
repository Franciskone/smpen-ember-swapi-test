import Service from '@ember/service';

export default Service.extend({
  
  playerOne: 0,
  playerTwo: 0,
  playerOneIncrease() {
    const newValue = this.get('playerOne') + 1;
    this.set('playerOne', newValue);
  },
  playerTwoIncrease() {
    const newValue = this.get('playerTwo') + 1;
    this.set('playerTwo', newValue);
  }
});
