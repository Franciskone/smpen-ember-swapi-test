import Route from '@ember/routing/route';
import {ROUTE_NAME} from "../router";

export default Route.extend({
  beforeModel() {
    this.replaceWith(`${ROUTE_NAME.PLAY}`);
  }
});
