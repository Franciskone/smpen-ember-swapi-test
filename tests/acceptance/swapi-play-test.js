import { module, test } from 'qunit';
import { visit, currentURL, click } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import Service from '@ember/service';

const peopleStub = Service.extend({
  peopleList: null,
  init() {
    this._super(...arguments);
    this.set('peopleList', []);
  },
  fillPeopleList() {
    this.set('peopleList', [
      {
        "name": "C-3PO",
        "height": "167",
        "mass": "75",
        "hair_color": "n/a",
        "skin_color": "gold",
        "eye_color": "yellow",
        "birth_year": "112BBY",
        "gender": "n/a",
        "homeworld": "https://swapi.co/api/planets/1/",
        "films": [
          "https://swapi.co/api/films/2/",
          "https://swapi.co/api/films/5/",
          "https://swapi.co/api/films/4/",
          "https://swapi.co/api/films/6/",
          "https://swapi.co/api/films/3/",
          "https://swapi.co/api/films/1/"
        ],
        "species": [
          "https://swapi.co/api/species/2/"
        ],
        "vehicles": [],
        "starships": [],
        "created": "2014-12-10T15:10:51.357000Z",
        "edited": "2014-12-20T21:17:50.309000Z",
        "url": "https://swapi.co/api/people/2/"
      },
      {
        "name": "Obi-Wan Kenobi",
        "height": "182",
        "mass": "77",
        "hair_color": "auburn, white",
        "skin_color": "fair",
        "eye_color": "blue-gray",
        "birth_year": "57BBY",
        "gender": "male",
        "homeworld": "https://swapi.co/api/planets/20/",
        "films": [
          "https://swapi.co/api/films/2/",
          "https://swapi.co/api/films/5/",
          "https://swapi.co/api/films/4/",
          "https://swapi.co/api/films/6/",
          "https://swapi.co/api/films/3/",
          "https://swapi.co/api/films/1/"
        ],
        "species": [
          "https://swapi.co/api/species/1/"
        ],
        "vehicles": [
          "https://swapi.co/api/vehicles/38/"
        ],
        "starships": [
          "https://swapi.co/api/starships/48/",
          "https://swapi.co/api/starships/59/",
          "https://swapi.co/api/starships/64/",
          "https://swapi.co/api/starships/65/",
          "https://swapi.co/api/starships/74/"
        ],
        "created": "2014-12-10T16:16:29.192000Z",
        "edited": "2014-12-20T21:17:50.325000Z",
        "url": "https://swapi.co/api/people/10/"
      }
    ])
  }
});
const starshipsStub = Service.extend({
  starshipsList: null,
  init() {
    this._super(...arguments);
    this.set('starshipsList', []);
  },
  fillStarshipsList() {
    this.set('starshipsList', [
      {
        "name": "C-3PO",
        "height": "167",
        "mass": "75",
        "hair_color": "n/a",
        "skin_color": "gold",
        "eye_color": "yellow",
        "birth_year": "112BBY",
        "gender": "n/a",
        "homeworld": "https://swapi.co/api/planets/1/",
        "films": [
          "https://swapi.co/api/films/2/",
          "https://swapi.co/api/films/5/",
          "https://swapi.co/api/films/4/",
          "https://swapi.co/api/films/6/",
          "https://swapi.co/api/films/3/",
          "https://swapi.co/api/films/1/"
        ],
        "species": [
          "https://swapi.co/api/species/2/"
        ],
        "vehicles": [],
        "starships": [],
        "created": "2014-12-10T15:10:51.357000Z",
        "edited": "2014-12-20T21:17:50.309000Z",
        "url": "https://swapi.co/api/people/2/"
      },
      {
        "name": "Obi-Wan Kenobi",
        "height": "182",
        "mass": "77",
        "hair_color": "auburn, white",
        "skin_color": "fair",
        "eye_color": "blue-gray",
        "birth_year": "57BBY",
        "gender": "male",
        "homeworld": "https://swapi.co/api/planets/20/",
        "films": [
          "https://swapi.co/api/films/2/",
          "https://swapi.co/api/films/5/",
          "https://swapi.co/api/films/4/",
          "https://swapi.co/api/films/6/",
          "https://swapi.co/api/films/3/",
          "https://swapi.co/api/films/1/"
        ],
        "species": [
          "https://swapi.co/api/species/1/"
        ],
        "vehicles": [
          "https://swapi.co/api/vehicles/38/"
        ],
        "starships": [
          "https://swapi.co/api/starships/48/",
          "https://swapi.co/api/starships/59/",
          "https://swapi.co/api/starships/64/",
          "https://swapi.co/api/starships/65/",
          "https://swapi.co/api/starships/74/"
        ],
        "created": "2014-12-10T16:16:29.192000Z",
        "edited": "2014-12-20T21:17:50.325000Z",
        "url": "https://swapi.co/api/people/10/"
      }
    ])
  }
});


module('Acceptance | swapi play', function(hooks) {
  setupApplicationTest(hooks);
  
  test('should show "PLAY" page as main page', async function (assert) {
    assert.expect(1);
    
    await visit('/');
    assert.equal(currentURL(), `/play`, 'should redirect automatically');
  });
  
  test('should contain header and footer in each route', async function (assert) {
    assert.expect(4);
  
    await visit('/');
    assert.ok(!!this.element.querySelector('.header-swapi'), 'header present in INDEX page');
    assert.ok(!!this.element.querySelector('footer.page-footer'), 'footer present in INDEX page');
  
    await visit('/play');
    assert.ok(!!this.element.querySelector('.header-swapi'), 'header present in PLAY page');
    assert.ok(!!this.element.querySelector('footer.page-footer'), 'footer present in PLAY page');
    
  });
  
  test('it shows loading while opponentsList is Empty', async function(assert) {
    assert.expect(3);
  
    this.owner.register('service:people', peopleStub);
    this.owner.register('service:starships', starshipsStub);
    let peopleService = this.owner.lookup('service:people');
    let starshipsService = this.owner.lookup('service:starships');
    
    peopleService.peopleList = [];
    starshipsService.starshipsList = [];
    
    await visit('/');
  
    assert.ok(!!this.element.querySelector('.preloader-wrapper .spinner-layer .circle-clipper.left .circle'));
    assert.ok(!!this.element.querySelector('.preloader-wrapper .spinner-layer .gap-patch .circle'));
    assert.ok(!!this.element.querySelector('.preloader-wrapper .spinner-layer .circle-clipper.right .circle'));
    
  });
  
  test('it shows start to play Call to Action after data fetchin success', async function (assert) {
    assert.expect(1);
    
    this.owner.register('service:people', peopleStub);
    this.owner.register('service:starships', starshipsStub);
    let peopleService = this.owner.lookup('service:people');
    let starshipsService = this.owner.lookup('service:starships');
    
    peopleService.fillPeopleList();
    starshipsService.fillStarshipsList();
    
    await visit('/');
    assert.ok(!!this.element.querySelector('.cta-container'));
    
  });
  
  test('it show game PEOPLE OPPONENT CARD after click a PEOPLE-PLAY button', async function(assert) {
    assert.expect(2);
    
    this.owner.register('service:people', peopleStub);
    this.owner.register('service:starships', starshipsStub);
    let peopleService = this.owner.lookup('service:people');
    let starshipsService = this.owner.lookup('service:starships');
  
    peopleService.fillPeopleList();
    starshipsService.fillStarshipsList();
    
    await visit('/');
    await click('.people-play-button');
    
    assert.ok(!!this.element.querySelector('.opponent-card'), 'Ok, opponent card is shown');
    assert.ok(!!this.element.querySelector('.people-card-detail'), 'Ok, it is a PEOPLE opponent card');
  });
  
  test('it show STARSHIPS result if click on STARSHIPS PLAY button', async function(assert) {
    assert.expect(2);
    
    this.owner.register('service:people', peopleStub);
    this.owner.register('service:starships', starshipsStub);
    let peopleService = this.owner.lookup('service:people');
    let starshipsService = this.owner.lookup('service:starships');
  
    peopleService.fillPeopleList();
    starshipsService.fillStarshipsList();
  
    await visit('/');
    await click('.starships-play-button');
  
    assert.ok(!!this.element.querySelector('.opponent-card'), 'Ok, opponent card is shown');
    assert.ok(!!this.element.querySelector('.starship-card-detail'), 'Ok, it is a STARSHIPS opponent card');
  });
  
});
