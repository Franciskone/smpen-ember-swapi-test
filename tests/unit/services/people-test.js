import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Service | people', function(hooks) {
  setupTest(hooks);

  test('it adds People', function(assert) {
    assert.expect(1);
    
    let service = this.owner.lookup('service:people');
    service.addPeople([1,2,3,4]);
    assert.equal(service.get('peopleList').length, 4);
  });
});

