import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Service | starships', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it adds Starships', function(assert) {
    assert.expect(1);
    
    let service = this.owner.lookup('service:starships');
    service.addStarships([1,2,3,4]);
    assert.equal(service.get('starshipsList').length, 4);
  });
});

