import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Service | wincounter', function(hooks) {
  setupTest(hooks);
  
  test('it increases players wins', function(assert) {
    assert.expect(2);
  
    let service = this.owner.lookup('service:wincounter');
    
    service.set('playerOne', 4);
    service.playerOneIncrease();
    assert.equal(service.get('playerOne'), 5, 'Player one increase OK');
  
    service.set('playerTwo', 4);
    service.playerTwoIncrease();
    assert.equal(service.get('playerTwo'), 5, 'Player two increase OK');
  });
});

