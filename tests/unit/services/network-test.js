import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Service | network', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    assert.expect(1);
  
    let service = this.owner.lookup('service:network');
    assert.ok(!!service);
  });
});

