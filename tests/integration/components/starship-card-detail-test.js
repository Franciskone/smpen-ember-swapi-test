import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | starship-card-detail', function(hooks) {
  setupRenderingTest(hooks);
  
  test('it shows correctly the winning chip', async function(assert) {
    assert.expect(2);
  
    // assert winning chip is present
      this.set('isWinner', true);
      await render(hbs`{{starship-card-detail
        isWinner=isWinner
      }}`);
      
      assert.ok(!!this.element.querySelector('.starship-card-detail .chip.winner'), 'winning chip correctly shown');
   
    // assert winning chip is not present
      this.set('isWinner', false);
      await render(hbs`{{starship-card-detail
        isWinner=isWinner
      }}`);
    
      assert.notOk(!!this.element.querySelector('.starship-card-detail .chip.winner'), 'winning chip correctly NOT shown');
  });
});
