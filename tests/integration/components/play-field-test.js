import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';


module('Integration | Component | play-field', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders loading component at the beginning', async function(assert) {
    assert.expect(1);
  
    await render(hbs`{{play-field}}`);
    assert.ok(this.element.textContent.trim().includes('Fetching Star Wars data ...'));
  });
});
