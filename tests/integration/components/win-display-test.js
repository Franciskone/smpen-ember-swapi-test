import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | win-display', function(hooks) {
  setupRenderingTest(hooks);

  test('it shows the given winning amount', async function(assert) {
    assert.expect(1);
    
    this.set('testVal', 5);
    await render(hbs`{{win-display amount=testVal}}`);
    assert.equal(this.element.textContent.trim(), 'Win: 5');
  });
});
