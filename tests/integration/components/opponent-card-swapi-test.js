import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | opponent-card-swapi', function(hooks) {
  setupRenderingTest(hooks);

  test('block usage NOT allowed', async function(assert) {
    assert.expect(1);
  
    // Template block usage:
    await render(hbs`
      {{#opponent-card-swapi}}
        template block text
      {{/opponent-card-swapi}}
    `);
    
    assert.notOk(this.element.textContent.trim().includes('template block text'), 'GREAT: it works !!!');
  });
  
  test('it shows correctly attributes', async function(assert) {
    assert.expect(2);
    
    const playerInfo = {
      "imageUrl": "https://cdn-images-1.medium.com/max/1200/0*VtQec_vkEa0kpkl7.png",
      "data": {
        "name": "R5-D4",
        "height": "97",
        "mass": "32",
        "hair_color": "n/a",
        "skin_color": "white, red",
        "eye_color": "red",
        "birth_year": "unknown",
        "gender": "n/a",
        "homeworld": "https://swapi.co/api/planets/1/",
        "films": [
          "https://swapi.co/api/films/1/"
        ],
        "species": [
          "https://swapi.co/api/species/2/"
        ],
        "vehicles": [],
        "starships": [],
        "created": "2014-12-10T15:57:50.959000Z",
        "edited": "2014-12-20T21:17:50.321000Z",
        "url": "https://swapi.co/api/people/8/"
      },
      "isWinner": false
    };
    
    this.set('playerInfo', playerInfo);
    this.set('isPeopleType', true);
    await render(hbs`{{opponent-card-swapi
      playerInfo=playerInfo
      isPeopleType=isPeopleType
    }}`);
    
    assert.equal(this.element.querySelector('.card-image .card-title').textContent.trim(), 'R5-D4', 'NAME TITLE correctly displayed');
    assert.equal(this.element.querySelector('.card-image img').getAttribute('src'), 'https://cdn-images-1.medium.com/max/1200/0*VtQec_vkEa0kpkl7.png', 'IMAGE correctly displayed');
  });
  
  test('should show loser overlay', async function(assert) {
    assert.expect(2);
    
    // Winner doesn't show overlay
      const playerInfoWinner = {
      "imageUrl": "https://cdn-images-1.medium.com/max/1200/0*VtQec_vkEa0kpkl7.png",
      "data": {
        "name": "R5-D4",
        "height": "97",
        "mass": "32",
        "hair_color": "n/a",
        "skin_color": "white, red",
        "eye_color": "red",
        "birth_year": "unknown",
        "gender": "n/a",
        "homeworld": "https://swapi.co/api/planets/1/",
        "films": [
          "https://swapi.co/api/films/1/"
        ],
        "species": [
          "https://swapi.co/api/species/2/"
        ],
        "vehicles": [],
        "starships": [],
        "created": "2014-12-10T15:57:50.959000Z",
        "edited": "2014-12-20T21:17:50.321000Z",
        "url": "https://swapi.co/api/people/8/"
      },
      "isWinner": true
    };
      this.set('playerInfo', playerInfoWinner);
      this.set('isPeopleType', true);
      await render(hbs`{{opponent-card-swapi
        playerInfo=playerInfo
        isPeopleType=isPeopleType
      }}`);
  
      assert.notOk(!!this.element.querySelector('.opponent-card .loser-overlay'), 'loser overlay correctly NOT shown');
  
    // Loser does show overlay
      const playerInfoLoser = {
      "imageUrl": "https://cdn-images-1.medium.com/max/1200/0*VtQec_vkEa0kpkl7.png",
      "data": {
        "name": "R5-D4",
        "height": "97",
        "mass": "32",
        "hair_color": "n/a",
        "skin_color": "white, red",
        "eye_color": "red",
        "birth_year": "unknown",
        "gender": "n/a",
        "homeworld": "https://swapi.co/api/planets/1/",
        "films": [
          "https://swapi.co/api/films/1/"
        ],
        "species": [
          "https://swapi.co/api/species/2/"
        ],
        "vehicles": [],
        "starships": [],
        "created": "2014-12-10T15:57:50.959000Z",
        "edited": "2014-12-20T21:17:50.321000Z",
        "url": "https://swapi.co/api/people/8/"
      },
      "isWinner": false
    };
      this.set('playerInfo', playerInfoLoser);
      this.set('isPeopleType', true);
      await render(hbs`{{opponent-card-swapi
        playerInfo=playerInfo
        isPeopleType=isPeopleType
      }}`);
    
      assert.ok(!!this.element.querySelector('.opponent-card .loser-overlay'), 'loser overlay correctly shown');
  });
});
