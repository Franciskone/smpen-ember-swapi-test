import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | loading-spinner', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders correctly', async function(assert) {
    assert.expect(3);

    await render(hbs`{{loading-spinner}}`);

    assert.ok(!!this.element.querySelector('.preloader-wrapper .spinner-layer .circle-clipper.left .circle'));
    assert.ok(!!this.element.querySelector('.preloader-wrapper .spinner-layer .gap-patch .circle'));
    assert.ok(!!this.element.querySelector('.preloader-wrapper .spinner-layer .circle-clipper.right .circle'));
    
  });
});
