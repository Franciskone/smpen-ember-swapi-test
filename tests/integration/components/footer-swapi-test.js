import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | footer-swapi', function(hooks) {
  setupRenderingTest(hooks);

  test('block usage NOT allowed', async function(assert) {
    assert.expect(1);
    
    // Template block usage:
    await render(hbs`
      {{#footer-swapi}}
        template block text
      {{/footer-swapi}}
    `);

    assert.ok(this.element.textContent.trim().includes(`template block text`), 'Template block usage is OK');
  });
});
