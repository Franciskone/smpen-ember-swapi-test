import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | header-swapi', function(hooks) {
  setupRenderingTest(hooks);

  test('does not support Block usage', async function(assert) {
    assert.expect(1);

    // Template block usage:
    await render(hbs`
      {{#header-swapi}}
        template block text
      {{/header-swapi}}
    `);

    assert.notOk(this.element.textContent.trim().includes('template block text'));
  });
});
