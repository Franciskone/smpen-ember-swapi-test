import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

const currentGamePlayerOneWins = {
  "isPeopleType": true,
  "playerOne": {
    "imageUrl": "https://cdn-images-1.medium.com/max/1200/0*VtQec_vkEa0kpkl7.png",
    "data": {
      "name": "Biggs Darklighter",
      "height": "183",
      "mass": "84",
      "hair_color": "black",
      "skin_color": "light",
      "eye_color": "brown",
      "birth_year": "24BBY",
      "gender": "male",
      "homeworld": "https://swapi.co/api/planets/1/",
      "films": [
        "https://swapi.co/api/films/1/"
      ],
      "species": [
        "https://swapi.co/api/species/1/"
      ],
      "vehicles": [],
      "starships": [
        "https://swapi.co/api/starships/12/"
      ],
      "created": "2014-12-10T15:59:50.509000Z",
      "edited": "2014-12-20T21:17:50.323000Z",
      "url": "https://swapi.co/api/people/9/"
    },
    "isWinner": true
  },
  "playerTwo": {
    "imageUrl": "http://3.bp.blogspot.com/-XO8s8Ax6M7w/UAZq3kTGK5I/AAAAAAAAA4I/hM77zr0rE40/s400/darth-vader-wallpaper-400x300.jpg",
    "data": {
      "name": "Obi-Wan Kenobi",
      "height": "182",
      "mass": "77",
      "hair_color": "auburn, white",
      "skin_color": "fair",
      "eye_color": "blue-gray",
      "birth_year": "57BBY",
      "gender": "male",
      "homeworld": "https://swapi.co/api/planets/20/",
      "films": [
        "https://swapi.co/api/films/2/",
        "https://swapi.co/api/films/5/",
        "https://swapi.co/api/films/4/",
        "https://swapi.co/api/films/6/",
        "https://swapi.co/api/films/3/",
        "https://swapi.co/api/films/1/"
      ],
      "species": [
        "https://swapi.co/api/species/1/"
      ],
      "vehicles": [
        "https://swapi.co/api/vehicles/38/"
      ],
      "starships": [
        "https://swapi.co/api/starships/48/",
        "https://swapi.co/api/starships/59/",
        "https://swapi.co/api/starships/64/",
        "https://swapi.co/api/starships/65/",
        "https://swapi.co/api/starships/74/"
      ],
      "created": "2014-12-10T16:16:29.192000Z",
      "edited": "2014-12-20T21:17:50.325000Z",
      "url": "https://swapi.co/api/people/10/"
    },
    "isWinner": false
  }
};
const currentGamePlayerTwoWins = {
  "isPeopleType": true,
  "playerOne": {
    "imageUrl": "https://cdn-images-1.medium.com/max/1200/0*VtQec_vkEa0kpkl7.png",
    "data": {
      "name": "R2-D2",
      "height": "96",
      "mass": "32",
      "hair_color": "n/a",
      "skin_color": "white, blue",
      "eye_color": "red",
      "birth_year": "33BBY",
      "gender": "n/a",
      "homeworld": "https://swapi.co/api/planets/8/",
      "films": [
        "https://swapi.co/api/films/2/",
        "https://swapi.co/api/films/5/",
        "https://swapi.co/api/films/4/",
        "https://swapi.co/api/films/6/",
        "https://swapi.co/api/films/3/",
        "https://swapi.co/api/films/1/",
        "https://swapi.co/api/films/7/"
      ],
      "species": [
        "https://swapi.co/api/species/2/"
      ],
      "vehicles": [],
      "starships": [],
      "created": "2014-12-10T15:11:50.376000Z",
      "edited": "2014-12-20T21:17:50.311000Z",
      "url": "https://swapi.co/api/people/3/"
    },
    "isWinner": false
  },
  "playerTwo": {
    "imageUrl": "http://3.bp.blogspot.com/-XO8s8Ax6M7w/UAZq3kTGK5I/AAAAAAAAA4I/hM77zr0rE40/s400/darth-vader-wallpaper-400x300.jpg",
    "data": {
      "name": "Darth Vader",
      "height": "202",
      "mass": "136",
      "hair_color": "none",
      "skin_color": "white",
      "eye_color": "yellow",
      "birth_year": "41.9BBY",
      "gender": "male",
      "homeworld": "https://swapi.co/api/planets/1/",
      "films": [
        "https://swapi.co/api/films/2/",
        "https://swapi.co/api/films/6/",
        "https://swapi.co/api/films/3/",
        "https://swapi.co/api/films/1/"
      ],
      "species": [
        "https://swapi.co/api/species/1/"
      ],
      "vehicles": [],
      "starships": [
        "https://swapi.co/api/starships/13/"
      ],
      "created": "2014-12-10T15:18:20.704000Z",
      "edited": "2014-12-20T21:17:50.313000Z",
      "url": "https://swapi.co/api/people/4/"
    },
    "isWinner": true
  }
};
const currentGameDraw = {
  "isPeopleType": true,
  "playerOne": {
    "imageUrl": "https://cdn-images-1.medium.com/max/1200/0*VtQec_vkEa0kpkl7.png",
    "data": {
      "name": "Beru Whitesun lars",
      "height": "165",
      "mass": "75",
      "hair_color": "brown",
      "skin_color": "light",
      "eye_color": "blue",
      "birth_year": "47BBY",
      "gender": "female",
      "homeworld": "https://swapi.co/api/planets/1/",
      "films": [
        "https://swapi.co/api/films/5/",
        "https://swapi.co/api/films/6/",
        "https://swapi.co/api/films/1/"
      ],
      "species": [
        "https://swapi.co/api/species/1/"
      ],
      "vehicles": [],
      "starships": [],
      "created": "2014-12-10T15:53:41.121000Z",
      "edited": "2014-12-20T21:17:50.319000Z",
      "url": "https://swapi.co/api/people/7/"
    },
    "isWinner": true
  },
  "playerTwo": {
    "imageUrl": "http://3.bp.blogspot.com/-XO8s8Ax6M7w/UAZq3kTGK5I/AAAAAAAAA4I/hM77zr0rE40/s400/darth-vader-wallpaper-400x300.jpg",
    "data": {
      "name": "C-3PO",
      "height": "167",
      "mass": "75",
      "hair_color": "n/a",
      "skin_color": "gold",
      "eye_color": "yellow",
      "birth_year": "112BBY",
      "gender": "n/a",
      "homeworld": "https://swapi.co/api/planets/1/",
      "films": [
        "https://swapi.co/api/films/2/",
        "https://swapi.co/api/films/5/",
        "https://swapi.co/api/films/4/",
        "https://swapi.co/api/films/6/",
        "https://swapi.co/api/films/3/",
        "https://swapi.co/api/films/1/"
      ],
      "species": [
        "https://swapi.co/api/species/2/"
      ],
      "vehicles": [],
      "starships": [],
      "created": "2014-12-10T15:10:51.357000Z",
      "edited": "2014-12-20T21:17:50.309000Z",
      "url": "https://swapi.co/api/people/2/"
    },
    "isWinner": true
  }
};

module('Integration | Component | play-results', function(hooks) {
  setupRenderingTest(hooks);

  test('Template block usage NOT available', async function(assert) {
    assert.expect(1);
  
    // Template block usage:
    await render(hbs`
      {{#play-results}}
        template block text
      {{/play-results}}
    `);

    assert.notOk(this.element.textContent.trim().includes('template block text'), 'Template block usage correclty NOT implemented');
  });
  
  test('it shows the correct winner', async function(assert) {
    assert.expect(6);
  
    this.set('playerOneWinAmount', 1);
    this.set('playerTwoWinAmount', 2);
  
    // player One wins
      this.set('currentGame', currentGamePlayerOneWins);
      await render(hbs`{{play-results
        playerOneWinAmount=playerOneWinAmount
        playerTwoWinAmount=playerTwoWinAmount
        currentGame=currentGame
      }}`);
    
      assert.ok(!!this.element.querySelector('.player-one .people-card-detail .chip.winner'), 'PLAYER ONE WINS: player one card has winning chip');
      assert.notOk(!!this.element.querySelector('.player-two .people-card-detail .chip.winner'), 'PLAYER ONE WINS: player two card has NOT winning chip');
  
    // player Two wins
      this.set('currentGame', currentGamePlayerTwoWins);
      await render(hbs`{{play-results
          playerOneWinAmount=playerOneWinAmount
          playerTwoWinAmount=playerTwoWinAmount
          currentGame=currentGame
        }}`);
    
      assert.notOk(!!this.element.querySelector('.player-one .people-card-detail .chip.winner'), 'PLAYER TWO WINS: player one card has NOT winning chip');
      assert.ok(!!this.element.querySelector('.player-two .people-card-detail .chip.winner'), 'PLAYER TWO WINS: player two card has winning chip');
      
    // // It's a Draw
      this.set('currentGame', currentGameDraw);
      await render(hbs`{{play-results
          playerOneWinAmount=playerOneWinAmount
          playerTwoWinAmount=playerTwoWinAmount
          currentGame=currentGame
        }}`);
    
      assert.ok(!!this.element.querySelector('.player-one .people-card-detail .chip.winner'), 'IT\' a DRAW: player one card has winning chip');
      assert.ok(!!this.element.querySelector('.player-two .people-card-detail .chip.winner'), 'IT\' a DRAW: player two card has winning chip');
  
  });
});
